package fr.bycode.app.jetpung.common.files;

import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class TestProducers {

    @Produces
    public JetInstance createInstane() {
        System.out.println("$$$$$$$$ TITI");
        return Jet.newJetInstance();
    }
}
