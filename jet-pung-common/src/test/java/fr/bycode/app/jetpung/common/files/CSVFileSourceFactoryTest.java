package fr.bycode.app.jetpung.common.files;

import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.pipeline.BatchSource;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import fr.bycode.app.jetpung.common.files.model.CSVFileLine;
import org.apache.meecrowave.junit.InjectRule;
import org.apache.meecrowave.junit.MeecrowaveRule;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVFileSourceFactoryTest {

    @ClassRule // started once for the class, @Rule would be per method
    public static final MeecrowaveRule RULE = new MeecrowaveRule();

    @Rule  // inject fields for each test
    public final TestRule injectRule = new InjectRule(this);

    @Inject
    private CSVFileSourceFactory csvFileSourceFactory;

    @Inject
    private JetInstance jetInstance;

    @Inject
    private JetFileService jetFileService;

    @Test
    public void test() throws IOException {
        // use "http://localhost:" + RULE.getConfiguration().getHttpPort()
        JetFileService jetFileService = CDI.current().select(JetFileService.class).get();
    }

    @Test
    public void test_normal_read() throws URISyntaxException {

        Path resPath = Paths.get(getClass().getResource("/file001.csv").toURI());

        File csvFile = resPath.toFile();

        BatchSource<CSVFileLine> source = csvFileSourceFactory.create(
                csvFile.getParentFile().getAbsolutePath(),
                csvFile.getName(),
                false,
                StandardCharsets.UTF_8,
                ",",
                true,
                null,
                false
        );

        Pipeline pipeline = Pipeline.create();

        pipeline
                .drawFrom(source)
                .drainTo(Sinks.list("output"));


        jetInstance.newJob(pipeline).join();


    }


}
