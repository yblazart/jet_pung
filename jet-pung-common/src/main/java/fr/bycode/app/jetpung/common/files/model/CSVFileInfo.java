package fr.bycode.app.jetpung.common.files.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CSVFileInfo {

    private String uuid;
    private String dir;
    private String fileName;
    private Long size;
    private List<String> headers;


}
