package fr.bycode.app.jetpung.common.files.event;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class CSVReadFileProgressEvent implements Serializable {

    private String fileInfoUUID;
    private long bytesReadied;
    private long totalSize;
    private long progress;

}
