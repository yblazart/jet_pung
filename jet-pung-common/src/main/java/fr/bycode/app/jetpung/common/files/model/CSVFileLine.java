package fr.bycode.app.jetpung.common.files.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class CSVFileLine implements Serializable {

    private Long lineNumber;
    private String fileInfoUUID;
    private String data[];

}
