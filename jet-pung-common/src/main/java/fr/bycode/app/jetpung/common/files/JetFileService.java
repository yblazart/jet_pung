package fr.bycode.app.jetpung.common.files;


import com.hazelcast.core.ReplicatedMap;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Job;
import com.hazelcast.jet.config.JobConfig;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sources;
import fr.bycode.app.jetpung.common.files.model.CSVFileInfo;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class JetFileService {


    @Inject
    private JetInstance jetInstance;

    @PostConstruct
    public void init() {
        //jetInstance.getHazelcastInstance()
        System.out.println("************ TOTO");
    }


    // -------------------
    // CSVFileInfo
    public void store(CSVFileInfo CSVFileInfo) {
        getFileInfoReplicatedMap().put(CSVFileInfo.getUuid(), CSVFileInfo,24, TimeUnit.HOURS);
    }

    private ReplicatedMap<String, CSVFileInfo> getFileInfoReplicatedMap() {
        return jetInstance.getReplicatedMap("__inner__FileInfoRepository");
    }

    public CSVFileInfo getCSVFileInfoByUUID(String uuid) {
       return getFileInfoReplicatedMap().get(uuid);

    }


}
