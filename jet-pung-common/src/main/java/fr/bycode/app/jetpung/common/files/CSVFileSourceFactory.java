package fr.bycode.app.jetpung.common.files;


import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.pipeline.BatchSource;
import com.hazelcast.jet.pipeline.Sources;
import fr.bycode.app.jetpung.common.files.event.CSVReadFileProgressEvent;
import fr.bycode.app.jetpung.common.files.model.CSVFileInfo;
import fr.bycode.app.jetpung.common.files.model.CSVFileLine;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.io.File;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.LongAdder;

@ApplicationScoped
public class CSVFileSourceFactory implements Serializable {

    public static final int DEFAULT_HEADERS_NUMBER = 20;

    @Inject
    private  JetFileService jetFileService;

    @Inject
    private  Event<CSVReadFileProgressEvent> csvReadFileProgressEventFire;


    /**
     * Create the source for a CSV file.
     * Simple parser. Do not handle complex csv
     * @param inputDir input directory
     * @param filePattern pattern for getting the file
     * @param sharedFS is this file shared by all nodes
     * @param charset charset of file
     * @param delimiter char delimiter for csv file
     * @param firstLineIsHeader if true, will compute and get the headers
     * @param headers if not firstLine is headers, set the headers
     * @return batch source
     */
    public BatchSource<CSVFileLine> create(String inputDir, String filePattern, boolean sharedFS, Charset charset, String delimiter, boolean firstLineIsHeader, List<String> headers, boolean skipHeaders) {

        jetFileService.toString();
        csvReadFileProgressEventFire.toString();
        final LongAdder lineCounter = new LongAdder();
        final LongAdder bytesReadied = new LongAdder();
        final List<String> computedHeaders = new ArrayList<>(headers!=null && headers.size()>0?headers.size(): DEFAULT_HEADERS_NUMBER);
        final AtomicReference<CSVFileInfo> csvFileInfoRef=new AtomicReference<>();
        final AtomicLong progress=new AtomicLong();

        if ( headers!=null && headers.size()>0) {
            computedHeaders.addAll(headers);
        }


        return Sources.filesBuilder(inputDir)
                .glob(filePattern)
                .charset(charset)
                .sharedFileSystem(sharedFS)
                .build((fileName, line) -> {

                    bytesReadied.add(line.length());



                    // compute headers if necessary
                    if (firstLineIsHeader && lineCounter.longValue() == 0 && computedHeaders.isEmpty()) {
                        StringTokenizer tokenizer=new StringTokenizer(line,delimiter,false);
                        int nbColumns = tokenizer.countTokens();
                        ((ArrayList<String>) computedHeaders).ensureCapacity(nbColumns);
                        while (tokenizer.hasMoreTokens()) {
                            computedHeaders.add(tokenizer.nextToken());
                        }
                    }


                    if (lineCounter.longValue() == 0) {
                        // On first line, set the CSVFileInfo in Map
                        CSVFileInfo fileInfo = CSVFileInfo.builder()
                                .dir(inputDir)
                                .uuid(UUID.randomUUID().toString())
                                .fileName(fileName)
                                .size(new File(inputDir, fileName).length())
                                .headers(computedHeaders)
                                .build();

                        jetFileService.store(fileInfo);
                        csvFileInfoRef.set(fileInfo);

                    }

                    //only fire event when % change
                    CSVFileInfo csvFileInfo = csvFileInfoRef.get();

                    long currentprogress = bytesReadied.longValue()*100/ csvFileInfo.getSize();
                    if ( currentprogress!=progress.longValue()) {
                        CSVReadFileProgressEvent event = CSVReadFileProgressEvent.builder()
                                .bytesReadied(bytesReadied.longValue())
                                .fileInfoUUID(csvFileInfo.getUuid())
                                .progress(currentprogress)
                                .totalSize(csvFileInfo.getSize())
                                .build();
                        csvReadFileProgressEventFire.fire(event);
                        progress.set(currentprogress);
                    }

                    if ( lineCounter.longValue()==0 && firstLineIsHeader && skipHeaders) {
                        lineCounter.increment();
                        return null;
                    }

                    String[] data = new String[computedHeaders.size()];

                    if ( lineCounter.longValue()==0 && firstLineIsHeader ) {
                        data = computedHeaders.toArray(data);
                    } else {
                        StringTokenizer tokenizer = new StringTokenizer(line,delimiter,false);
                        int index =0;
                        while ( index < computedHeaders.size() && tokenizer.hasMoreTokens()) {
                            data[index++] = tokenizer.nextToken();
                        }
                    }

                    CSVFileLine fileLine = CSVFileLine.builder()
                            .fileInfoUUID(csvFileInfo.getUuid())
                            .lineNumber(lineCounter.longValue())
                            .data(data)
                            .build();


                    lineCounter.increment();

                    return  fileLine;
                });


    }

}
